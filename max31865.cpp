#include "max31865.h"

using namespace adc;

bool Max31865::setConversionMode(ConversionMode cmode)
{
    // Ensure VBias is enabled if continuous conversion mode is selected. If not left configuration unchanged.
    if (cmode == ConversionMode::continuous && isVBiasEnabled() == false)
        return false;
    cmode_ = cmode;
    if (cmode_ == ConversionMode::continuous)
        config_register_ |= Configuration::conversionMode;
    else
        config_register_ &= static_cast<std::uint8_t>(~Configuration::conversionMode);
    writeConfigRegister(config_register_);
    return true;
}

void Max31865::triggerConversion()
{
    // If continuous conversion mode, no need to trigger conversion
    if (getConversionMode() == ConversionMode::continuous)
        return;

    // get a copy of current configurationsince one shot bit is self clearing
    auto config = config_register_;
    config |= Configuration::oneShot;
    writeConfigRegister(config);
    // adjust delay according to the filter type
    delay_.ms((filter_ == Filter::_50hz ? 66 : 55)); // A one shot conversion may take at maximum 66ms with 50Hz filter
                                                     // and 55ms with 60Hz filter
}

std::uint16_t Max31865::readRtd()
{
    // Trigger a conversion if necessary
    triggerConversion();

    std::array<std::uint8_t, 3> tx_buf, rx_buf;

    // Read the rtd data registers (rtd LSBs and rtd MSBs)
    tx_buf[0] = RegisterAddress::rtdMsbRead;
    spi_.select(true);
    spi_.transfer(std::move(tx_buf), rx_buf);
    spi_.select(false);

    // rx_buf[1] holds RTD MSBs and rx_buf[2] holds RTD LSBs whith the fault bit D0
    std::uint16_t rtd_value = static_cast<std::uint16_t>((rx_buf[1] << 8) | rx_buf[2]);
    // RQ: shift right here, check for fault ???? -> No, let the user check for fault.
    return rtd_value;
}

std::uint8_t Max31865::readFaults() const
{
    std::array<std::uint8_t, 2> tx_buf, rx_buf;
    tx_buf[0] = RegisterAddress::faultStatusRead;
    spi_.select(true);
    spi_.transfer(std::move(tx_buf), rx_buf);
    spi_.select(false);
    return rx_buf[1];
}