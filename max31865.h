/**
 * @file Max31865.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef MAX31865_H
#define MAX31865_H

#include <cstdint>

#include "tortilla.h"

namespace adc
{

    /**
     * @class Max31865
     * @brief 
     * 
     */
    class Max31865
    {

    public:
        static constexpr float absolute_zero = -273.15f; /// Absolute zero in celsius
        /**
         * @brief Number of wires used to connect the RTD
         * 
         */
        enum class Wires : std::uint8_t
        {
            two   = 0,
            three = 1,
            four  = 0
        };

        enum class ConversionMode : std::uint8_t
        {
            oneShot = 0,
            continuous
        };

        enum class Filter : std::uint8_t
        {
            _50hz,
            _60hz
        };

        /**
         * @brief Construct a new Max 3 1 8 6 5 object
         * 
         * @param rref_ohms 
         * @param cmode 
         * @param spi 
         * @param delay 
         */
        Max31865(float       rref_ohms,
                 tia::SPI &  spi,
                 tia::Delay &delay) : spi_{spi}, delay_{delay}, rref_ohms_{rref_ohms},
                                      wires_{Wires::four}, cmode_{ConversionMode::oneShot}, filter_{Filter::_60hz},
                                      vbias_enabled_{false}, low_threshold_{0x0000}, high_threshold_{0xFFFF}, config_register_{0x00}
        {
            // spi_.enableAutoCs(true); // setup automatic CS handling
        }

        Max31865(tia::SPI &  spi,
                 tia::Delay &delay) : Max31865(0.f, spi, delay)
        {
        }

        /**
		 * @brief 
		 * 
		 * @param ohms 
		 */
        void setRRef(float ohms) { rref_ohms_ = ohms; }

        /**
		 * @brief Set the wire mode
		 * 
		 * @param wires 
		 */
        void setWires(Wires wires)
        {
            wires_ = wires;
            if (wires_ == Wires::three)
                config_register_ |= Configuration::threeWire; // enable three wire mode
            else
                config_register_ &= static_cast<std::uint8_t>(~Configuration::threeWire);
            writeConfigRegister(config_register_);
        }

        /**
		 * @brief 	Set the Conversion Mode
		 * @note	If you try to switch on continuous conversion mode while VBias is disabled
		 * 			this function will return immediately without changing the conversion mode.
		 * @param cmode 
		 * @return true 	New conversion mode successfully set up
		 * @return false 	Failed to setup new conversion mode 
		 */
        bool setConversionMode(ConversionMode cmode);

        /**
		 * @brief Set the Filter type
		 * 
		 * @param f 
		 */
        void setFilter(Filter f)
        {
            filter_ = f;
            if (filter_ == Filter::_50hz)
                config_register_ |= Configuration::filter50hz60hzSelect; // enable 50Hz filter
            else
                config_register_ &= static_cast<std::uint8_t>(~Configuration::filter50hz60hzSelect);
            writeConfigRegister(config_register_);
        }

        /**
		 * @brief 	Enable or disable VBias. When enabled, Vbias might source up to 5.75mA depending
		 * 			on the reference resistor value.
		 * @note 	Disabling VBias in continuous conversion mode will result in bad rtd values.
		 * 
		 * @param enable 
		 */
        void enableVBias(bool enable)
        {
            vbias_enabled_ = enable;
            // Set or unset vbias bit in config register
            if (vbias_enabled_ == true)
                config_register_ |= Configuration::vbias;
            else
                config_register_ &= static_cast<std::uint8_t>(~Configuration::vbias);
            writeConfigRegister(config_register_);
        }

        /**
		 * @brief 	Set the Low Treshold value
		 * @note 	This function checks whether low treshold isn't higher than high treshold
		 * 
		 * @param l_tresh Low threshold value
		 */
        void setLowTreshold(std::uint16_t l_tresh)
        {
            low_threshold_ = l_tresh < high_threshold_ ? l_tresh : low_threshold_; // check low treshold isn't higher than high treshold
            spi_.select(true);
            spi_.write(std::array<std::uint8_t, 3>{RegisterAddress::lowFaultTreshMsbWrite,
                                                   static_cast<std::uint8_t>(low_threshold_ >> 8),
                                                   static_cast<std::uint8_t>(low_threshold_ & 0xFF)});
            spi_.select(false);
        }

        /**
		 * @brief 	Set the High Treshold value
		 * @note  	This function checks whether high treshold isn't lower than low treshold
		 * 
		 * @param h_tresh High threshold value
		 */
        void setHighTreshold(std::uint16_t h_tresh)
        {
            high_threshold_ = h_tresh > low_threshold_ ? h_tresh : high_threshold_; // check high treshold isn't lower than low treshold
            spi_.select(true);
            spi_.write(std::array<std::uint8_t, 3>{RegisterAddress::highFaultTreshMsbWrite,
                                                   static_cast<std::uint8_t>(high_threshold_ >> 8),
                                                   static_cast<std::uint8_t>(high_threshold_ & 0xFF)});
            spi_.select(false);
        }

        /**
		 * @brief 	Get the wire mode.
		 * 
		 * @return 	Wires The current number of wires selected
		 */
        Wires getWires() const { return wires_; }

        /**
		 * @brief Get the conversion mode
		 * 
		 * @return ConversionMode The current conversion mode selected
		 */
        ConversionMode getConversionMode() const { return cmode_; }

        /**
		 * @brief Get the filter type enabled
		 * 
		 * @return Filter The filter type enabled
		 */
        Filter getFilter() const { return filter_; }

        /**
		 * @brief  Get the state of vbias.
		 * 
		 * @return true if vbias is ON
		 * @return false if vbias is OFF
		 */
        bool isVBiasEnabled() const { return vbias_enabled_; }

        /**
		 * @brief 	Triggers a conversion cycle.
		 * @note 	This function may block approximately 65ms in one shot mode
		 * 			but returns immediately with no effect in continuous 
		 * 			conversion mode. You can safely call it in both conversion modes.
		 */
        void triggerConversion();

        /**
		 * @brief 	Read the RTD register raw value. If one shot conversion mode is selected,
		 * 			this method triggers a conversion before reading the actual RTD registers raw value.
		 * 			If continuous conversion mode is selected, this method simply reads the RTD 
		 * 			registers raw value.
		 * @note 	In one shot conversion mode, remember to enable VBias before calling this function.
		 * @note 	The Lsb (D0) of the returned value is the "Fault" bit.
		 * @return 	std::uint16_t The actual RTD registers raw value
		 */
        std::uint16_t readRtd();

        /**
		 * @brief 	Get the Temperature in deg celsius.
		 * 
 		 * @note 	This function reads rtd value and performs some calculation
		 * 			to convert the value in Ohms As with readRtd(), if one shot conversion mode
		 * 			is selected remember to enable VBias before calling this function.
		 * 
		 * @return 	int the RTD value in Ohms or a negative value if a fault is present.
		 */
        float readRtdOhms()
        {
            auto rawRtd = readRtd();
            if (rawRtd & 1) // if there is a fault, return error value
                return -128.f;
            else
                return static_cast<float>(rawRtd >> 1) * rref_ohms_ / 32768.f;
        }

        /**
		 * @brief 	Read fault status register.
		 * @note 	This function doesn't perform any fault detection cycle.
		 * 			It only reads fault status register.
		 * @return std::uint8_t 
		 */
        std::uint8_t readFaults() const;

        /**
		 * @brief 	Clear fault status register.
		 * 
		 */
        void clearFaults()
        {
            auto config = config_register_; // retreive the config register value;
            // reset D5 (oneShot), D3 (faultStatusCycleControl3) and D2 (faultStatusCycleControl2)
            config &= static_cast<std::uint8_t>(~(Configuration::oneShot | Configuration::faultDetectionCycleControl3 | Configuration::faultDetectionCycleControl2));
            // and set D1 (faultStatusClear)
            config |= Configuration::faultStatusClear;
            writeConfigRegister(config);
        }

    private:
        // temperature curve polynomial approximation coefficients
        static constexpr float a1 = 2.55865721669f;
        static constexpr float a2 = 9.67360412e-04f;
        static constexpr float a3 = 7.31467e-07f;
        static constexpr float a4 = 6.91e-10f;
        static constexpr float a5 = 7.31888555389e-13f;

        // Registers addresses mapping (read and write address)
        enum RegisterAddress : std::uint8_t
        {
            // Read addresses
            configRead = 0x00,
            rtdMsbRead,
            rtdLsbRead,
            highFaultTreshMsbRead,
            highFaultTreshLsbRead,
            lowFaultTreshMsbRead,
            lowFaultTreshLsbRead,
            faultStatusRead,
            // Write addresses
            configWrite            = 0x80,
            highFaultTreshMsbWrite = 0x83,
            highFaultTreshLsbWrite,
            lowFaultTreshMsbWrite,
            lowFaultTreshLsbWrite
        };

        // Configuration register flags definition
        enum Configuration : std::uint8_t
        {
            filter50hz60hzSelect        = 0x01,
            faultStatusClear            = 0x02,
            faultDetectionCycleControl2 = 0x04,
            faultDetectionCycleControl3 = 0x08,
            threeWire                   = 0x10,
            oneShot                     = 0x20,
            conversionMode              = 0x40,
            vbias                       = 0x80
        };

        // Write config register shorthand
        void writeConfigRegister(std::uint8_t config)
        {
            spi_.select(true);
            spi_.write(std::array<std::uint8_t, 2>{RegisterAddress::configWrite, config});
            spi_.select(false);
        }

        // Platform specific
        tia::SPI &  spi_;   //< SPI wrapper
        tia::Delay &delay_; //< Delay wrapper

        // config
        float          rref_ohms_;      //< Reference resistance in ohms (recommended on Max31865 datasheet: 400 ohms for PT100, 4000 for PT1000)
        Wires          wires_;          //< Rtd number of wires
        ConversionMode cmode_;          //< Conversion mode used
        Filter         filter_;         //< Filter used
        bool           vbias_enabled_;  //< Bias enabled or not
        std::uint16_t  low_threshold_;  //< Low fault threshold
        std::uint16_t  high_threshold_; //< High fault threshold

        std::uint8_t config_register_; //< The config register value. This backup avoids reading config register
                                       /// before modifying it, at the cheap cost of 1 more byte in memory.
    };

} // namespace device

#endif