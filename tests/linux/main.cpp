/**
 * @file main.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2021-04-27
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <cstdint>
#include <iostream>

#include "max31865.h"
#include "rtd.h"
#include "tortilla.h"

// Tortilla wrapper functions

// Delays
void myDelayUs([[maybe_unused]] uint32_t us)
{
    std::cout << __func__ << "(" << us << ") invoked\n";
}

void myDelayMs([[maybe_unused]] uint32_t ms)
{
    std::cout << __func__ << "(" << ms << ") invoked\n";
}

// SPI
void mySpiCs([[maybe_unused]] bool enable)
{
    std::cout << __func__ << "(" << enable << ") invoked\n";
}

int mySpiTransfer([[maybe_unused]] const std::uint8_t *tx_buf,
                  [[maybe_unused]] std::size_t         tx_size,
                  [[maybe_unused]] std::uint8_t *      rx_buf,
                  [[maybe_unused]] std::size_t         rx_size)
{
    std::cout << __func__ << "(...," << tx_size << ',' << "...," << rx_size << ") invoked\n";

    std::array<std::uint8_t, 3> spi_values{0x00, 0x5D, 0x7C};

    for (std::size_t i = 0; i < std::min(rx_size, spi_values.size()); i++)
        rx_buf[i] = spi_values[i];

    return 0;
}

// end Tortilla wrapper functions

int main()
{
    // Prepare tortilla wrappers
    tia::Delay delay{myDelayMs, myDelayUs}; // Delay wrapper
    tia::SPI   spi{mySpiTransfer, mySpiCs, true};

    // Setup max31865
    constexpr float rrefOhms = 400.f; // Reference resistor is 400 Ohms (for this example)

    adc::Max31865 max{rrefOhms, spi, delay};

    max.setFilter(adc::Max31865::Filter::_60hz);
    max.setLowTreshold(0x0000);
    max.setHighTreshold(0xFFFF);
    max.setWires(adc::Max31865::Wires::four);
    max.setConversionMode(adc::Max31865::ConversionMode::oneShot);

    //
    max.enableVBias(true);
    delay.ms(2);
    [[maybe_unused]] auto rdt_raw = max.readRtd();
    max.enableVBias(false);

    max.readFaults();
    max.clearFaults();

    max.enableVBias(true);
    delay.ms(2);
    auto ohms = max.readRtdOhms();
    max.enableVBias(false);

    auto temp = rtd::getTemperatureCelsius<rtd::PT100>(ohms);
    std::cout << "PT100 value <" << ohms << "Ohms> converted to <" << temp << "°C>\n";

    ohms = 103.90f;
    temp = rtd::getTemperatureCelsius<rtd::PT100>(ohms); // Should get 10°C
    std::cout << "PT100 value <" << ohms << "Ohms> converted to <" << temp << "°C>\n";

    return 0;
}