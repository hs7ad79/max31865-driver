/**
 * @file main.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2021-04-27
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <cstdint>
#include <cstring>

#include "app_util_platform.h"
#include "boards.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrfx_spim.h"

#include "max31865.h"
#include "rtd.h"
#include "tortilla.h"

/****************************************/
/* SPIM0 Setup to be used with MAX31865 */
/*                                      */

// redefine pins here according to your board
#define MISO_PIN     NRFX_SPIM_PIN_NOT_USED
#define MOSI_PIN     NRFX_SPIM_PIN_NOT_USED
#define SCK_PIN      NRFX_SPIM_PIN_NOT_USED
#define CS_PIN       NRFX_SPIM_PIN_NOT_USED

#define SPI_INSTANCE 0                                                       /// SPI instance index
static const nrfx_spim_t spim0_instance  = NRFX_SPIM_INSTANCE(SPI_INSTANCE); /// Get SPI instance
static volatile bool     spim0_xfer_done = false;                            /// Flag used to indicate that SPI instance completed the transfer

/**
 * @brief SPIM0 event handler 
 */
void spim0_event_handler(nrfx_spim_evt_t const *p_event, void *p_context)
{
    switch (p_event->type)
    {
    case NRFX_SPIM_EVENT_DONE:
        spim0_xfer_done = true;
        break;

    default:
        break;
    }
}

/**
 * @brief Set the up SPI0 Instance to be used with Max31865
 * 
 */
void setup_spim0()
{
    // Slave select pin, active low
    nrf_gpio_cfg_output(CS_PIN);
    nrf_gpio_pin_set(CS_PIN);

    nrfx_spim_config_t spim0_config = NRFX_SPIM_DEFAULT_CONFIG;

    spim0_config.frequency          = NRF_SPIM_FREQ_2M; // MAX31865 supports up to 5Mhz
    spim0_config.miso_pin           = MISO_PIN;
    spim0_config.mosi_pin           = MOSI_PIN;
    spim0_config.sck_pin            = SCK_PIN;
    spim0_config.ss_pin             = NRFX_SPIM_PIN_NOT_USED;
    spim0_config.mode               = NRF_SPIM_MODE_1; // MAX31865 works in mode 1 and 3

    // configure non-blocking spi transfer
    nrfx_spim_init(&spim0_instance, &spim0_config, spim0_event_handler, NULL);
}

/************************************/
/* Setup Tortilla wrapper functions */
/*                                  */

// Delays
void nordic_delay_us(uint32_t us)
{
    nrf_delay_us(us);
}

void nordic_delay_ms(uint32_t ms)
{
    nrf_delay_ms(ms);
}

// SPI

/**
 * @brief Chip select control 
 */
void nordic_spim_cs(bool enable)
{
    nrf_gpio_pin_write(CS_PIN, enable);
}

/**
 * @brief SPIM0 data transfer
 */
int nordic_spim_transfer(const std::uint8_t *tx_buf, size_t tx_size, std::uint8_t *rx_buf, size_t rx_size)
{
    int res                         = 0;
    spim0_xfer_done                 = false; // Reset transfer finished flag
    nrfx_spim_xfer_desc_t xfer_desc = {.p_tx_buffer = tx_buf,
                                       .tx_length   = tx_size,
                                       .p_rx_buffer = rx_buf,
                                       .rx_length   = rx_size};
    res                             = nrfx_spim_xfer(&spim0_instance, &xfer_desc, 0);
    while (!spim0_xfer_done)
    {
        __WFE();
    } // Sleep until event occurs
    return res;
}
// end Tortilla wrapper functions

// Max31865 static hardware configuration

int main()
{
    constexpr float rref_ohms = 430.f; /// We use a 430Ohms reference resistor

    // Prepare peripherals
    ::setup_spim0();

    // Prepare tortilla wrappers
    tia::Delay delay{::nordic_delay_ms /*delay milliseconds*/, ::nordic_delay_us /*delay microseconds*/};                       // Delay wrapper
    tia::SPI   spi{::nordic_spim_transfer /*spi transfer*/, ::nordic_spim_cs /*chip select*/, true /*enable CS auto control*/}; // SPI wrapper

    // Prepare max31865
    adc::Max31865 max{rref_ohms, spi, delay};

    // Setup Max31865
    max.setFilter(adc::Max31865::Filter::_60hz);
    max.setLowTreshold(0x0000);
    max.setHighTreshold(0xFFFF);
    max.setWires(adc::Max31865::Wires::four);
    max.setConversionMode(adc::Max31865::ConversionMode::oneShot);

    // Use Max31865

    // Read RTD value
    max.enableVBias(true);
    delay.ms(2);                           // should wait 10.5 times RC time constant + 1ms after enabling VBias
    volatile auto rdt_raw = max.readRtd(); // made volatile to be readeable in debug session
    max.enableVBias(false);

    while (1)
    {
        // Read RTD value and convert it into temperature
        max.enableVBias(true);
        delay.ms(2);
        auto ohms = max.readRtdOhms();
        max.enableVBias(false);

        auto temp = rtd::getTemperatureCelsius<rtd::PT100>(ohms);

        // Temperature shouldn't reach negative values (in this example)
        if (temp < -10.f)
        {
            // Read fault status
            volatile auto faults = max.readFaults(); // made volatile to be readeable in debug session
            (void)faults;
            max.clearFaults();
        }
    }
}
